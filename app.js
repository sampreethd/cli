var app = require('express')();
const chalk = require('chalk');
var figlet = require('figlet');
var clearTerminal = require('clear');
var readline = require('readline');

// const log = console.log;
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: `sam:> `
});

figlet(`SAMPREETH's CLI`, (err, res) => {
    if (err) { console.log('error') }

    else {
        console.log(chalk.black.bold.bgYellow(res));
        menu()
    }
})

function menu() {
    let menu = ['Help (1)', 'Collect my Details (2)'];

    console.log(chalk.blackBright.bold.bgWhiteBright(`THINGS YOU CAN DO IN SAMPREETH'S CLI : `))
    menu.forEach((menuItems) => {
        console.log(chalk.blackBright.bold.bgWhiteBright(menuItems));
    })

    rl.prompt();
    rl.on('line', (line) => {
        if (line == 1) {
            let executeOptions = ['1.String Length(sl)'];
            console.log(chalk.blackBright.bold.bgWhiteBright(`Options you can execute : `));
            executeOptions.forEach((opt) => {
                console.log(chalk.blueBright.bgGrey(`${opt}`));
            })
            let sl = rl.question('enter your option: ', (option) => {
                if (option == 'sl') {
                    stringLength();

                }
                else {
                    console.log('sorry wrong input!!!');
                    clearTerminal();
                    exitCli()
                }
            })
            exitCli()
        }
        else if (line == 2) {
            let userObj = {}
            let personalQuestion = ['Enter your Name: ', 'Enter your Age: ', 'Enter Your Gender (M/F) :']
            rl.question(`${chalk.underline.italic.bgGreenBright.black(`Enter your Name: `)}`, (ans) => {
                console.log(chalk.underline.bgCyan(`Welcome ${ans}`));
                userObj['name'] = ans;

                rl.question(`${chalk.underline.italic.bgGreenBright.black('Enter your age : ')}`, (ans) => {
                    userObj['age'] = ans;

                    rl.question('Do you want to verify your details?(Y/N) ', (ans) => {
                        if (ans.toUpperCase() == 'Y') {
                            console.log(chalk.bgGrey.blueBright(`Name : ${userObj['name']} Age : ${userObj['age']}`));
                            exitCli()
                        }
                        else {
                            exitCli();
                        }
                    })
                })

            })

        }
    })
}


function stringLength() {
    rl.question('Enter your string: ', (ans) => {
        console.log(chalk.bgBlackBright.whiteBright(`String Length : ${ans.length}`));
        rl.question('continue ??(yes/no) ', (ans) => {
            if (ans.toUpperCase() == 'YES') {
                stringLength()
            }
            else if (ans.toUpperCase() == 'NO') {
                menu()
            }
            else {
                rl.close()
            }
        })

    })

}

function exitCli() {
    rl.question('exit?(Y/N)', (ans) => {
        if (ans.toUpperCase() == 'Y') {
            console.log(figlet('Bella Chaio', (err, res) => {
                if (err) { console.log(chalk.bgBlack.whiteBright('error', err)) }
                else { console.log(chalk.bgBlack.whiteBright(res)) }
            }))
        }
        else {
            rl.close();
            rl.prompt();
        }
        rl.close()
        rl.prompt()
    })
}


