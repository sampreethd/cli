## COMMAND LINE INTERFACE TRY USING NODEJS

INSTALL REQUIRED PACKAGES BY DOING THE FOLLOWING STEPS :

1. Clone the project by using **git clone** command followed by repository link.
2. **npm i** to install required packages from package.json.
3. Run the project by the command **nodemon app.js** or **node app.js**

